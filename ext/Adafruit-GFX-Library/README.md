`gfxfont.h` is taken from the [*Adafruit-GFX-Library*](https://github.com/adafruit/Adafruit-GFX-Library)
`LiberationMono_Bold7pt7b.h` was created using the [`fontconvert`](https://github.com/adafruit/Adafruit-GFX-Library/tree/master/fontconvert) utility on the [*Liberation Mono*](https://github.com/liberationfonts) font.
