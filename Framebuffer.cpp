/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
*/

#include "Framebuffer.h"
#include <string.h>

Framebuffer::Framebuffer() {
    this->clear();
}

#ifndef SIMULATOR
void Framebuffer::drawBitmap(const uint8_t *progmem_bitmap, uint8_t height, uint8_t width, uint8_t pos_x, uint8_t pos_y) {
    uint16_t bit_pos = 0;
    uint8_t current_byte = 0;
    for (uint8_t y = 0; y<height; y++) {
        for (uint8_t x=0; x<width; x++) {
            if (!(bit_pos & 0x7)) {
                current_byte = pgm_read_byte(progmem_bitmap++);
            }
            drawPixel(pos_x + x, pos_y + y, (current_byte & (0x80 >> (bit_pos & 0x7)) ? 1 : 0));
            bit_pos++;
        }
    }


}

void Framebuffer::putText(uint8_t x, uint8_t const y, GFXfont const & font, char const * text) {
    GFXglyph glyph;
    while (*text) {
        memcpy_P(&glyph, font.glyph + *text - font.first, sizeof(GFXglyph));
        drawBitmap(
                font.bitmap + glyph.bitmapOffset
                , glyph.height
                , glyph.width
                , x + glyph.xOffset
                , y + glyph.yOffset
                );
        x += glyph.xAdvance;
        text++;
    }
}

void Framebuffer::drawBuffer(const uint8_t *progmem_buffer) {
    uint8_t current_byte;

    for (uint8_t y_pos = 0; y_pos < 64; y_pos++) {
        for (uint8_t x_pos = 0; x_pos < 128; x_pos++) {
            current_byte = pgm_read_byte(progmem_buffer + y_pos*16 + x_pos/8);
            if (current_byte & (128 >> (x_pos&7))) {
                this->drawPixel(x_pos,y_pos,1);
            } else {
                this->drawPixel(x_pos,y_pos,0);
            }
        }
    }
}
#endif

void Framebuffer::drawPixel(
        uint8_t const x
        , uint8_t const y
        , uint8_t const c
        )
{
    if (x >= SSD1306_WIDTH || y >= SSD1306_HEIGHT) {
        return;
    }
    switch(c) {
        case Framebuffer::WT :
            buffer[x+(y/8)*SSD1306_WIDTH] |= (1 << (y&7));
            break;
        case Framebuffer::BK :
            buffer[x+(y/8)*SSD1306_WIDTH] &= ~(1 << (y&7));
    }
}

void Framebuffer::drawVLine(
        uint8_t const x
        , uint8_t const y0
        , uint8_t const l
        , uint8_t const c
        )
{
    uint8_t const y1 = y0 + l;
    for (uint8_t y = y0; y < y1; ++y) {
        drawPixel(x, y, c);
    }
}

void Framebuffer::drawHLine(
        uint8_t const x0
        , uint8_t const y
        , uint8_t const l
        , uint8_t const c
        )
{
    uint8_t const x1 = x0 + l;
    for (uint8_t x = x0; x < x1; ++x) {
        drawPixel(x , y, c);
    }
}

void Framebuffer::drawRect(
        uint8_t const x
        , uint8_t const y
        , uint8_t const w
        , uint8_t const h
        , uint8_t const c
        )
{
    drawHLine(x,   y,   w, c);
    drawHLine(x,   y+h, w, c);
    drawVLine(x,   y,   h, c);
    drawVLine(x+w, y,   h, c);
}

void Framebuffer::fillRect(
        uint8_t const x
        , uint8_t const y
        , uint8_t const w
        , uint8_t const h
        , uint8_t const c
        )
{
    uint8_t const y1 = y+h;
    for (uint8_t yy=y; yy<y1; ++yy) {
        drawHLine(x, yy, w, c);
    }
}

void Framebuffer::clear() {
    memset(buffer, 0, SSD1306_BUFFERSIZE);
}

void Framebuffer::invert(uint8_t status) {
    oled.invert(status);
}

void Framebuffer::show() {
    oled.sendFramebuffer(buffer);
}

