/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
*/

#include "I2C.h"

I2C::I2C(uint8_t const address)
    : address(address)
{
    TWSR = 0;
    TWBR = ((F_CPU/SCL_CLOCK)-16)/2;
}

inline bool I2C::wait_timed_out() const {
    uint16_t timeout = 0;
    while(++timeout && !(TWCR & (1<<TWINT)));
    if (!timeout) TWCR = 0;
    return !timeout;
}

uint8_t I2C::start() const
{
    TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
    if (wait_timed_out()) return 1;

    {
        uint8_t const status = TW_STATUS & 0xF8;
        if ((status != TW_START) && (status != TW_REP_START)) {
            return 1;
        }
    }

    TWDR = address;
    TWCR = (1<<TWINT) | (1<<TWEN);
    if (wait_timed_out()) return 1;

    {
        uint8_t const status = TW_STATUS & 0xF8;
        if ((status != TW_MT_SLA_ACK) && (status != TW_MR_SLA_ACK)) {
            return 1;
        }
    }
    return 0;
}

uint8_t I2C::write(uint8_t const data) const
{
    TWDR = data;
    TWCR = (1<<TWINT) | (1<<TWEN);
    if (wait_timed_out()) return 1;

    if ((TW_STATUS & 0xF8) != TW_MT_DATA_ACK) {
        return 1;
    } else {
        return 0;
    }
}

void I2C::stop(void) const
{
    TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
}
