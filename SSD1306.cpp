/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
*/

#include <stdint.h>
#include "SSD1306.h"

#ifdef SIMULATOR
#include "simulator/I2C.h"
#else
#include "I2C.h"
#endif

SSD1306::SSD1306()
    : i2c(SSD1306_DEFAULT_ADDRESS)
{
    // Turn display off
    sendCommand(SSD1306_DISPLAYOFF);

    sendCommand({SSD1306_SETDISPLAYCLOCKDIV, 0x80});

    sendCommand({SSD1306_SETMULTIPLEX, 0x3F});
    
    sendCommand({SSD1306_SETDISPLAYOFFSET,0x00});
    
    sendCommand(SSD1306_SETSTARTLINE | 0x00);
    
    // We use internal charge pump
    sendCommand({SSD1306_CHARGEPUMP, 0x14});
    
    // Horizontal memory mode
    sendCommand({SSD1306_MEMORYMODE, 0x00});
    
    sendCommand(SSD1306_SEGREMAP | 0x1);

    sendCommand(SSD1306_COMSCANDEC);

    sendCommand({SSD1306_SETCOMPINS, 0x12});

    // Max contrast
    sendCommand({SSD1306_SETCONTRAST, 0xCF});

    sendCommand({SSD1306_SETPRECHARGE, 0xF1});

    sendCommand({SSD1306_SETVCOMDETECT, 0x40});

    sendCommand(SSD1306_DISPLAYALLON_RESUME);

    // Non-inverted display
    sendCommand(SSD1306_NORMALDISPLAY);

    // Turn display back on
    sendCommand(SSD1306_DISPLAYON);
}

void SSD1306::sendCommand(uint8_t const command) const
{
    while(1) {
        if (i2c.start()) break;
        if (i2c.write(0x00)) break;
        if (i2c.write(command)) break;
        i2c.stop(); break;
    }
}

void SSD1306::sendCommand(std::initializer_list<uint8_t> const command) const
{
    while(1) {
        if (i2c.start()) break;
        if (i2c.write(0x00)) break;
        bool loop_ok = true;
        for (auto it = command.begin(); it != command.end(); ++it) {
            if (i2c.write(*it)) {
                loop_ok=false;
                break;
            }
        }
        if (!loop_ok) break;
        i2c.stop(); break;
    }
}

void SSD1306::sendData(uint8_t const data) const
{
    while(1) {
        if (i2c.start()) break;
        if (i2c.write(0x40)) break;
        if (i2c.write(data)) break;
        i2c.stop(); break;
    }
}

void SSD1306::sendData(uint8_t const * const data, uint8_t const len) const
{
    while(1) {
        if (i2c.start()) break;
        if (i2c.write(0x40)) break;
        bool loop_ok = true;
        for (uint8_t i=0; i<len; i++) {
            if (i2c.write(data[i])) {
                loop_ok=false;
                break;
            }
        }
        if (!loop_ok) break;
        i2c.stop(); break;
    }
}

void SSD1306::invert(uint8_t inverted) const
{
    if (inverted) {
        sendCommand(SSD1306_INVERTDISPLAY);
    } else {
        sendCommand(SSD1306_NORMALDISPLAY);
    }
}

void SSD1306::sendFramebuffer(uint8_t *buffer) const
{
    sendCommand({SSD1306_COLUMNADDR, 0x00, 0x7F});
    sendCommand({SSD1306_PAGEADDR, 0x0, 0x07});

    // We have to send the buffer as 16 bytes packets
    // Our buffer is 1024 bytes long, 1024/16 = 64
    // We have to send 64 packets
    
    for (uint8_t packet = 0; packet < 64; packet++) {
        sendData(buffer + 16*packet, 16);
    }
    
}
